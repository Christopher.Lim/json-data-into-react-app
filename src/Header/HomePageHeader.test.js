import { render, screen } from '@testing-library/react';
import HomePageHeader from './HomePageHeader';

beforeEach(() => {
  render(<HomePageHeader />);
});

test('render HomePageHeader', () => {
  expect(screen.getByText('Your Stock Tracker')).toBeInTheDocument();
});

test('check for the visibility of the header text', () => {
  expect(screen.getByText('Your Stock Tracker')).toBeVisible();
  expect(screen.getByTestId('header')).toBeVisible();
});

test('check for the class attribute', () => {
  expect(screen.getByTestId('header')).toHaveClass('header');
});
