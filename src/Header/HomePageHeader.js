import React from "react";
import './HomePageHeader.css';

function HomePageHeader() {
  return (
    <header data-testid='header' className='header'>
      <h2>Your Stock Tracker</h2>
    </header>
  )
}

export default HomePageHeader;
