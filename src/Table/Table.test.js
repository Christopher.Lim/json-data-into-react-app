import { render, screen } from "@testing-library/react";
import Table from './Table';


test('render Table', () => {
    render(<Table company='Lim' ticker='LIM' stockPrice='1234 USD' timeElapsed='999 sec ago' />);
    expect(screen.getByTestId('table')).toBeVisible();
    expect(screen.getByTestId('company')).toBeVisible();
    expect(screen.getByTestId('ticker')).toBeVisible();
    expect(screen.getByTestId('stockPrice')).toBeVisible();
    expect(screen.getByTestId('timeElapsed')).toBeVisible();
});

test('table contains the correct text', () => {
    render(<Table company='Lim' ticker='LIM' stockPrice='1234 USD' timeElapsed='999 sec ago' />);
    expect(screen.getByTestId('company')).toHaveTextContent('Lim');
    expect(screen.getByTestId('ticker')).toHaveTextContent('LIM');
    expect(screen.getByTestId('stockPrice')).toHaveTextContent('1234 USD');
    expect(screen.getByTestId('timeElapsed')).toHaveTextContent('999 sec ago');
});

test('table does not contain anything', () => {
    render(<Table />);
    expect(screen.getByTestId('emptyTable')).toBeInTheDocument();
});
