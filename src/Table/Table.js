import React from 'react';
import './Table.css';

function Table({ company, ticker, stockPrice, timeElapsed }) {
  if (!company) return <div data-testid='emptyTable' />;
  return (
    <table data-testid='table'>
      <tbody>
        <tr>
          <td>
            <h5 data-testid='company'>{company}</h5>
          </td>
          <td>
            <h5 data-testid='ticker'>{ticker}</h5>
          </td>
          <td>
            <h4 data-testid='stockPrice'>{stockPrice}</h4>
          </td>
          <td>
            <p data-testid='timeElapsed'>{timeElapsed}</p>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default Table;
