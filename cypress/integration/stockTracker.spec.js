import { stockTrackerPage }  from '../support/index';

describe('React Sample Test Suite', () => {
  before(() => {
    cy.navigate();
  })

  it('should load the page components', () => {
    stockTrackerPage.getRoot().should('be.visible');
    stockTrackerPage.getHeader().should('be.visible');
    stockTrackerPage.getTable().should('be.visible');
    stockTrackerPage.getCompany().should('be.visible');
    stockTrackerPage.getTicker().should('be.visible');
    stockTrackerPage.getStockPrice().should('be.visible');
    stockTrackerPage.getTimeElapsed().should('be.visible');
  });
});
